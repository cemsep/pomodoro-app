import React from 'react';
import { Button } from 'react-bootstrap';
import * as renderIf from 'render-if';

class Timer extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      isPomodoro: false, 
      isBreak: false, 
      minutes: null, 
      seconds: null}
  }

  startPomodoro() {
    this.setState({isPomodoro: true, minutes: 25, seconds: 0})
    const { minutes, seconds } = this.state
    this.startTimer(minutes, seconds)
  }

  startBreak() {
    this.setState({isBreak: true, minutes: 10, seconds: 0})
    const { minutes, seconds } = this.state
    this.startTimer(minutes, seconds)
  }

  stopPomodoro() {
    this.setState({isPomodoro: false})
    this.stopTimer();
  }

  stopBreak() {
    this.setState({isBreak: false})
    this.stopTimer();
  }

  startTimer(minutes, seconds) {
    this.timer = setInterval(() => {
      const { minutes, seconds } = this.state

      if (seconds > 0) {
        this.setState(({seconds}) => ({
          seconds: seconds - 1
        }))
      }

      if (seconds === 0) {
        if (minutes === 0) {
          this.setState({isPomodoro: false, isBreak: false})
          clearInterval(this.timer)
        } else {
          this.setState(({minutes}) => ({
            minutes: minutes - 1,
            seconds: 59
          }))
        }
      }
    }, 1000)
  }

  stopTimer() {
    clearInterval(this.timer)
  }

  render() {

    const { isPomodoro, isBreak, minutes, seconds } = this.state

    let startPomodoro = (!this.state.isPomodoro && !this.state.isBreak) ? 
      <Button variant="primary" onClick={this.startPomodoro.bind(this)}>Start Pomodoro</Button> : null

    let stopPomodoro = (!this.state.isPomodoro) ? null :
      <Button variant="danger" onClick={this.stopPomodoro.bind(this)}>Stop</Button>

    let startBreak = (!this.state.isBreak && !this.state.isPomodoro) ? 
      <Button variant="secondary" onClick={this.startBreak.bind(this)}>Start Break</Button> : null

    let stopBreak = (!this.state.isBreak) ? null :
      <Button variant="danger" onClick={this.stopBreak.bind(this)}>Stop</Button>

    return (
      <div>
        {renderIf(isPomodoro || isBreak)(() => (
          <div>
            <h3>Time Remaining: {minutes}:{seconds < 10 ? `0${seconds}` : seconds}</h3>
          </div>
        ))}
        <span>{startPomodoro} {startBreak}</span>
        {stopPomodoro}
        {stopBreak}
      </div>
    )
  }
}
export default Timer;